-- this file was manually created
INSERT INTO public.users (display_name, email, handle, cognito_user_id)
VALUES
  ('Andrew Brown','andrew@exampro.co' , 'andrewbrown' ,'9f3e7e8e-dfbb-403e-a2c9-e2b8f5a03a51'),
  ('Andrew Bayko','bayko@exampro.co' , 'bayko' ,'4fe8e38f-7422-4373-9d6f-405475c87594'),
  ('test kipassa','noreply.sii.test@gmail.com' ,'hirmano' ,'0576d4f5-52cd-4f54-88f0-857219c862d2');

INSERT INTO public.activities (user_uuid, message, expires_at)
VALUES
  (
    (SELECT uuid from public.users WHERE users.handle = 'andrewbrown' LIMIT 1),
    'This was imported as seed data!',
    current_timestamp + interval '10 day'
  )