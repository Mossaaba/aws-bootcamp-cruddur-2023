
sgr-0442b3e48b2ab2d70

sg-043bcc1b2b1fb6bc7



export GITPOD_IP=$(curl ifconfig.me)
gp env GITPOD_IP=$(curl ifconfig.me)


echo $GITPOD_IP


export DB_SG_ID="sg-043bcc1b2b1fb6bc7"
gp env DB_SG_ID="sg-043bcc1b2b1fb6bc7"

export DB_SG_RULE_ID="sgr-0442b3e48b2ab2d70"
gp env DB_SG_RULE_ID="sgr-0442b3e48b2ab2d70"



aws ec2 modify-security-group-rules \
    --group-id $DB_SG_ID \
    --security-group-rules "SecurityGroupRuleId=$DB_SG_RULE_ID,SecurityGroupRule={Description=GITPOD,IpProtocol=tcp,FromPort=5432,ToPort=5432,CidrIpv4=$GITPOD_IP/32}"