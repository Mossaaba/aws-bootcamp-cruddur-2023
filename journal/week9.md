# Week 9 — CI/CD with CodePipeline, CodeBuild and CodeDeploy

### Prerequisites

***CodePipeline is*** a continuous delivery service provided by Amazon Web Services (AWS) that helps you automate the release process for your software applications. It allows you to build, test, and deploy your applications quickly and reliably.
Create a pipemline 

- Create a new branch from main called "prod"
- Create a buildspec.yml
- Create backend-flask/buildspec.yml
- Pull Request from "main" to "prod" branch with the pushed updates after creating the file


  
  
## Configuring CodeBuild
  
#### Create a uild project 
 - Name: cruddur-backend-flask-bake-image
 - Enable build badge
 - Source:
Source provider: GitHub (Connect to GitHub if asked)
Repository URL: https://github.com/<Your Github account>/aws-bootcamp-cruddur-202
  
####Environment:
- Operating system: Amazon Linux2
- Runtime(s): Standard
- Image:latest image
- Check the "Privileged" option

####Service Role (Role name will be auto generated)
  
- timeout = 15 minutes
  
####buildspec > Buildspec name: backend-flask/buildspec.yml

####CloudWatch logs:

- Group name: /cruddur/build/backend-flask
- Stream name: backend-flask
  
  ![Screenshot 2023-06-10 at 12 38 52](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/35eaef01-9b66-4e8c-a756-bdb5304f59c1)


## Configuring CodePipeline

***Create a pipeline*** : 
- Name: cruddur-backend-fargate
- Create a github connection:
- Source provider: GitHub
![Screenshot 2023-06-10 at 11 32 28](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/931e82f3-6bdd-4c3b-a21b-f0dc1825637a)
- Install the application https://github.com/<Your Github account>/aws-bootcamp-cruddur-2023
![Screenshot 2023-06-10 at 11 39 35](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/7d59bb07-563c-412c-88dc-039fe790a589)
- Use skip stage (We will handle the pipeline with code)
- Use ECS as a Deploy provider 
- Create the pipeline
  ![image](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/bfeb11c2-fb4f-46a8-a70f-847697f33ebd)

#### Start build
  
I got a permissions error to login to ECR, So I added the required permissions into the created IAM Role for the codebuild project.

Build stage succeeded!
  ![Screenshot 2023-06-10 at 14 02 13](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/7ac23bb7-bead-4f1a-bb9b-04d11846c865)

### Edit Pipeline 
  - Use the output of the build as artifact to use it in the deploy setup
  ![Screenshot 2023-06-10 at 15 40 03](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/f175dae6-663b-4a8f-b9da-84a3e4fbb6e0)
<img width="1680" alt="Screenshot 2023-06-10 at 15 40 22" src="https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/e8e05a0e-9aa3-406f-a135-37c9013dffee">

  
  
  - Add Healthcheck 
  <img width="575" alt="Screenshot 2023-06-10 at 14 52 08" src="https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/32d22cee-a938-477e-8745-5d3de47ad0be">
