# Week 6 — Deploying Containers

## Test RDS Connection

```py
#!/usr/bin/env python3

import psycopg
import os
import sys

connection_url = os.getenv("CONNECTION_URL")

conn = None
try:
  print('attempting connection')
  conn = psycopg.connect(connection_url)
  print("Connection successful!")
except psycopg.Error as e:
  print("Unable to connect to the database:", e)
finally:
  conn.close()
```

![Screenshot 2023-05-07 at 00 10 45](https://user-images.githubusercontent.com/11331502/236648496-5bad7c6a-2b72-4ccf-91d1-7f121ab09442.png)

---

### Implementing health check into the flask app
```py
@app.route('/api/health-check')
def health_check():
  return {'success': True}, 200
```
Create new scripts at backend-flask/bin/health-check
```py
#!/usr/bin/env python3

import urllib.request

try:
  response = urllib.request.urlopen('http://localhost:4567/api/health-check')
  if response.getcode() == 200:
    print("[OK] Flask server is running")
    exit(0) # success
  else:
    print("[BAD] Flask server is not running")
    exit(1) # false
# This for some reason is not capturing the error....
#except ConnectionRefusedError as e:
# so we'll just catch on all even though this is a bad practice
except Exception as e:
  print(e)
  exit(1) # false
```
![Screenshot 2023-05-07 at 00 16 51](https://user-images.githubusercontent.com/11331502/236648622-6e3f6c4c-540f-4674-b032-1a42f38b38d7.png)

---

### Create a cloud watch group :
```sh
aws logs create-log-group --log-group-name cruddur
aws logs put-retention-policy --log-group-name cruddur --retention-in-days 1
```
<img width="1680" alt="Screenshot 2023-04-29 at 11 17 19" src="https://user-images.githubusercontent.com/11331502/235315009-a154c12b-c3b1-4c5c-9d18-e7047165d20f.png">

---
### Provision ECS Cluster

Create ECS cluster cruddur using AWS CLI

```sh 
aws ecs create-cluster \
--cluster-name cruddur \
--service-connect-defaults namespace=cruddur
```
<img width="1680" alt="Screenshot 2023-04-29 at 11 21 04" src="https://user-images.githubusercontent.com/11331502/235315023-11a099e5-42ba-4bd7-976c-c3c665f91542.png">
<img width="1680" alt="Screenshot 2023-04-29 at 19 19 17" src="https://user-images.githubusercontent.com/11331502/235315490-95fa106f-376f-4bda-a827-c57d75223229.png">

--- 
### ECR  

## Create Repository
```sh
aws ecr create-repository \
  --repository-name cruddur-python \
  --image-tag-mutability MUTABLE
```

<img width="1680" alt="Screenshot 2023-04-29 at 19 18 00" src="https://user-images.githubusercontent.com/11331502/235315422-2abb804c-f6b0-406e-9267-8dbba01e34f5.png">
<img width="1680" alt="Screenshot 2023-04-29 at 19 18 48" src="https://user-images.githubusercontent.com/11331502/235315466-fa314003-da0d-425c-8f10-dbefe9e79c03.png">


## Login to ECR

```sh
aws ecr get-login-password --region $AWS_DEFAULT_REGION | docker login --username AWS --password-stdin "$AWS_ACCOUNT_ID.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com"
```
![Screenshot 2023-05-07 at 00 23 25](https://user-images.githubusercontent.com/11331502/236648756-4948b31d-5f7f-4264-8970-63e6f9307fad.png)

>>  Base-Image Python 

```.sh
aws ecr create-repository \
  --repository-name cruddur-python \
  --image-tag-mutability MUTABLE
```

#### Set URL
```.sh
export ECR_PYTHON_URL="$AWS_ACCOUNT_ID.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com/cruddur-python"
echo $ECR_PYTHON_URL
```
#### Pull Image
```.sh
docker pull python:3.10-slim-buster
```
#### Tag Image
```.sh
docker tag python:3.10-slim-buster $ECR_PYTHON_URL:3.10-slim-buster
```
##### Push Image
```.sh
docker push $ECR_PYTHON_URL:3.10-slim-buster
```
<img width="1680" alt="Screenshot 2023-05-07 at 00 29 47" src="https://user-images.githubusercontent.com/11331502/236648885-27a34c26-2ace-4233-9303-fca9049eb5c2.png">


>>  Backend 
Create Repository
 
```.sh
aws ecr create-repository \
  --repository-name backend-flask \
  --image-tag-mutability MUTABLE
```
<img width="1680" alt="Screenshot 2023-05-07 at 00 32 14" src="https://user-images.githubusercontent.com/11331502/236648967-a99261f1-0dee-4555-a7b7-c6098c464a88.png">



#### Set URL
```.sh
export ECR_BACKEND_FLASK_URL="$AWS_ACCOUNT_ID.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com/backend-flask"
echo $ECR_BACKEND_FLASK_URL
```
#### Build 
```.sh
#! /usr/bin/bash

ABS_PATH=$(readlink -f "$0")
BACKEND_PATH=$(dirname $ABS_PATH)
BIN_PATH=$(dirname $BACKEND_PATH)
PROJECT_PATH=$(dirname $BIN_PATH)
BACKEND_FLASK_PATH="$PROJECT_PATH/backend-flask"

docker build \
-f "$BACKEND_FLASK_PATH/Dockerfile.prod" \
-t backend-flask-prod \
"$BACKEND_FLASK_PATH/."
```
#### Tag
```.sh
docker tag backend-flask:latest $ECR_BACKEND_FLASK_URL:latest
```
#### Push
```.sh
#! /usr/bin/bash

ECR_BACKEND_FLASK_URL="$AWS_ACCOUNT_ID.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com/backend-flask"
echo $ECR_BACKEND_FLASK_URL

docker tag backend-flask-prod:latest $ECR_BACKEND_FLASK_URL:latest
docker push $ECR_BACKEND_FLASK_URL:latest
```

<img width="1680" alt="Screenshot 2023-05-07 at 00 41 15" src="https://user-images.githubusercontent.com/11331502/236649218-873358ec-05f5-4ae8-b541-ff71488c2548.png">

### Register (If we modify the task definitions)

```sh
#! /usr/bin/bash

ABS_PATH=$(readlink -f "$0")
FRONTEND_PATH=$(dirname $ABS_PATH)
BIN_PATH=$(dirname $FRONTEND_PATH)
PROJECT_PATH=$(dirname $BIN_PATH)
TASK_DEF_PATH="$PROJECT_PATH/aws/task-defintions/backend-flask.json"

echo $TASK_DEF_PATH

aws ecs register-task-definition \
--cli-input-json "file://$TASK_DEF_PATH"
```

#### Deploy 

```sh
#! /usr/bin/bash

CLUSTER_NAME="cruddur"
SERVICE_NAME="backend-flask"
TASK_DEFINTION_FAMILY="backend-flask"


LATEST_TASK_DEFINITION_ARN=$(aws ecs describe-task-definition \
--task-definition $TASK_DEFINTION_FAMILY \
--query 'taskDefinition.taskDefinitionArn' \
--output text)

echo "TASK DEF ARN:"
echo $LATEST_TASK_DEFINITION_ARN

aws ecs update-service \
--cluster $CLUSTER_NAME \
--service $SERVICE_NAME \
--task-definition $LATEST_TASK_DEFINITION_ARN \
--force-new-deployment
```

<img width="1680" alt="Screenshot 2023-05-07 at 00 42 20" src="https://user-images.githubusercontent.com/11331502/236649243-f4bcc5b0-6d15-4bed-9de6-ea2dd496a01c.png">


#### Run with PRODUCTION 

```sh
#! /usr/bin/bash

CLUSTER_NAME="cruddur"
SERVICE_NAME="backend-flask"
TASK_DEFINTION_FAMILY="backend-flask"


LATEST_TASK_DEFINITION_ARN=$(aws ecs describe-task-definition \
--task-definition $TASK_DEFINTION_FAMILY \
--query 'taskDefinition.taskDefinitionArn' \
--output text)

echo "TASK DEF ARN:"
echo $LATEST_TASK_DEFINITION_ARN

aws ecs update-service \
--cluster $CLUSTER_NAME \
--service $SERVICE_NAME \
--task-definition $LATEST_TASK_DEFINITION_ARN \
--force-new-deployment
```




>>  Frontend 

#### Frontend React Repo
Create Repository

```.sh
aws ecr create-repository \
  --repository-name frontend-react-js \
  --image-tag-mutability MUTABLE
```
##### Set URL

```.sh
export ECR_FRONTEND_REACT_URL="$AWS_ACCOUNT_ID.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com/frontend-react-js"
echo $ECR_FRONTEND_REACT_URL
```

##### Build Image
```.sh

#! /usr/bin/bash

ABS_PATH=$(readlink -f "$0")
FRONTEND_PATH=$(dirname $ABS_PATH)
BIN_PATH=$(dirname $FRONTEND_PATH)
PROJECT_PATH=$(dirname $BIN_PATH)
FRONTEND_REACT_JS_PATH="$PROJECT_PATH/frontend-react-js"

docker build \
--build-arg REACT_APP_BACKEND_URL="https://api.shopili.net" \
--build-arg REACT_APP_AWS_PROJECT_REGION="$AWS_DEFAULT_REGION" \
--build-arg REACT_APP_AWS_COGNITO_REGION="$AWS_DEFAULT_REGION" \
--build-arg REACT_APP_AWS_USER_POOLS_ID="*******************" \
--build-arg REACT_APP_CLIENT_ID="*******************" \
-t frontend-react-js \
-f "$FRONTEND_REACT_JS_PATH/Dockerfile.prod" \
"$FRONTEND_REACT_JS_PATH/."
```


##### Push
```.sh
#! /usr/bin/bash


ECR_FRONTEND_REACT_URL="$AWS_ACCOUNT_ID.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com/frontend-react-js"
echo $ECR_FRONTEND_REACT_URL

docker tag frontend-react-js:latest $ECR_FRONTEND_REACT_URL:latest
docker push $ECR_FRONTEND_REACT_URL:latest
```
<img width="1680" alt="Screenshot 2023-05-07 at 00 48 24" src="https://user-images.githubusercontent.com/11331502/236649355-aa72ecf0-8683-4608-821f-c9f7af7bb2ad.png">

####  Register 

```.sh
#! /usr/bin/bash

ABS_PATH=$(readlink -f "$0")
BACKEND_PATH=$(dirname $ABS_PATH)
BIN_PATH=$(dirname $BACKEND_PATH)
PROJECT_PATH=$(dirname $BIN_PATH)
TASK_DEF_PATH="$PROJECT_PATH/aws/task-definitions/frontend-react-js.json"

echo $TASK_DEF_PATH

aws ecs register-task-definition \
--cli-input-json "file://$TASK_DEF_PATH"
```

##### Deploy
```.sh
#! /usr/bin/bash

CLUSTER_NAME="cruddur"
SERVICE_NAME="frontend-react-js"
TASK_DEFINTION_FAMILY="frontend-react-js"

LATEST_TASK_DEFINITION_ARN=$(aws ecs describe-task-definition \
--task-definition $TASK_DEFINTION_FAMILY \
--query 'taskDefinition.taskDefinitionArn' \
--output text)

echo "TASK DEF ARN:"
echo $LATEST_TASK_DEFINITION_ARN

aws ecs update-service \
--cluster $CLUSTER_NAME \
--service $SERVICE_NAME \
--task-definition $LATEST_TASK_DEFINITION_ARN \
--force-new-deployment
```
<img width="1675" alt="Screenshot 2023-05-07 at 00 47 58" src="https://user-images.githubusercontent.com/11331502/236649346-fc887841-8ba1-4f6b-a315-d751ff255b1f.png">


#### Run 
```.sh
#! /usr/bin/bash

ABS_PATH=$(readlink -f "$0")
BACKEND_PATH=$(dirname $ABS_PATH)
BIN_PATH=$(dirname $BACKEND_PATH)
PROJECT_PATH=$(dirname $BIN_PATH)
ENVFILE_PATH="$PROJECT_PATH/frontend-react-js.env"

docker run --rm \
  --env-file $ENVFILE_PATH \
  --network cruddur-net \
  --publish 4567:4567 \
  -it frontend-react-js-prod
  
  ```
  
  ---
# Deploy Backend Flask app as a service to Fargate
---
### Create Task and Execution Roles for Task Defintion

#### Service Execution role

* Create `service-assume-role-execution-policy.json` inside `aws/policies`


```json
{
  "Version":"2012-10-17",
  "Statement":[{
      "Action":["sts:AssumeRole"],
      "Effect":"Allow",
      "Principal":{
        "Service":["ecs-tasks.amazonaws.com"]
    }}]
}
```

#### Create `service-execution-policy.json` inside `aws/policies`  
```json
{
  "Version": "2012-10-17",
  "Statement": [
      {
          "Sid": "VisualEditor0",
          "Effect": "Allow",
          "Action": [
              "ssm:GetParameters",
              "ssm:GetParameter",
              "ecr:GetAuthorizationToken",
              "ecr:BatchCheckLayerAvailability",
              "ecr:GetDownloadUrlForLayer",
              "ecr:BatchGetImage",
              "logs:CreateLogStream",
              "logs:PutLogEvents"
          ],
          "Resource": "*"
      }
  ]
}
```


#### Create *CruddurServiceExecutionRole* from terminal cli


```sh
aws iam create-role \
--role-name CruddurServiceExecutionRole \
--assume-role-policy-document file://aws/policies/service-assume-role-execution-policy.json
```

#### Create *CruddurServiceExecutionPolicy* from terminal cli

```sh
aws iam put-role-policy \
--policy-name CruddurServiceExecutionPolicy \
--role-name CruddurServiceExecutionRole \
--policy-document file://aws/policies/service-execution-policy.json`
```
* attach policy from terminal cli
```sh
aws iam attach-role-policy --policy-arn arn:aws:iam::${AWS_ACCOUNT_ID}:policy/CruddurServiceExecutionPolicy --role-name CruddurServiceExecutionRole
```

#### Task role

* Create task role from terminal cli
```sh
aws iam create-role \
    --role-name CruddurTaskRole \
    --assume-role-policy-document "{
  \"Version\":\"2012-10-17\",
  \"Statement\":[{
    \"Action\":[\"sts:AssumeRole\"],
    \"Effect\":\"Allow\",
    \"Principal\":{
      \"Service\":[\"ecs-tasks.amazonaws.com\"]
    }
  }]
}"
```

#### Put the policy to the role
```sh
aws iam put-role-policy \
  --policy-name SSMAccessPolicy \
  --role-name CruddurTaskRole \
  --policy-document "{
  \"Version\":\"2012-10-17\",
  \"Statement\":[{
    \"Action\":[
      \"ssmmessages:CreateControlChannel\",
      \"ssmmessages:CreateDataChannel\",
      \"ssmmessages:OpenControlChannel\",
      \"ssmmessages:OpenDataChannel\"
    ],
    \"Effect\":\"Allow\",
    \"Resource\":\"*\"
  }]
}
"
```
* Attach policies to the role
```sh
aws iam attach-role-policy --policy-arn arn:aws:iam::aws:policy/CloudWatchFullAccess --role-name CruddurTaskRole
```
```sh
aws iam attach-role-policy --policy-arn arn:aws:iam::aws:policy/AWSXRayDaemonWriteAccess --role-name CruddurTaskRole
```


---
#### Create task definition

* Create folder named `task-definitions` inside `aws/`  
* In this folder, Create a new file `backend-flask.json` 


```json
{
  "family": "backend-flask",
  "executionRoleArn": "arn:aws:iam::${AWS_ACCOUNT_ID}:role/CruddurServiceExecutionRole",
  "taskRoleArn": "arn:aws:iam::${AWS_ACCOUNT_ID}:role/CruddurTaskRole",
  "networkMode": "awsvpc",
  "cpu": "256",
  "memory": "512",
  "requiresCompatibilities": [ 
    "FARGATE" 
  ],
  "containerDefinitions": [
    {
      "name": "backend-flask",
      "image": "BACKEND_FLASK_IMAGE_URL",
      "essential": true,
      "healthCheck": {
        "command": [
          "CMD-SHELL",
          "python /backend-flask/bin/flask/health-check"
        ],
        "interval": 30,
        "timeout": 5,
        "retries": 3,
        "startPeriod": 60
      },
      "portMappings": [
        {
          "name": "backend-flask",
          "containerPort": 4567,
          "protocol": "tcp", 
          "appProtocol": "http"
        }
      ],
      "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
            "awslogs-group": "cruddur",
            "awslogs-region": "$AWS_DEFAULT_REGION",
            "awslogs-stream-prefix": "backend-flask"
        }
      },
      "environment": [
        {"name": "OTEL_SERVICE_NAME", "value": "backend-flask"},
        {"name": "OTEL_EXPORTER_OTLP_ENDPOINT", "value": "https://api.honeycomb.io"},
        {"name": "AWS_COGNITO_USER_POOL_ID", "value": "${AWS_COGNITO_USER_POOL_ID}"},
        {"name": "AWS_COGNITO_USER_POOL_CLIENT_ID", "value": "${AWS_COGNITO_USER_POOL_CLIENT_ID}"},
        {"name": "FRONTEND_URL", "value": "*"},
        {"name": "BACKEND_URL", "value": "*"},
        {"name": "AWS_DEFAULT_REGION", "value": "us-east-1"}
      ],
      "secrets": [
        {"name": "AWS_ACCESS_KEY_ID"    , "valueFrom": "arn:aws:ssm:us-east-1:${AWS_ACCOUNT_ID}:parameter/cruddur/backend-flask/AWS_ACCESS_KEY_ID"},
        {"name": "AWS_SECRET_ACCESS_KEY", "valueFrom": "arn:aws:ssm:us-east-1:${AWS_ACCOUNT_ID}:parameter/cruddur/backend-flask/AWS_SECRET_ACCESS_KEY"},
        {"name": "CONNECTION_URL"       , "valueFrom": "arn:aws:ssm:us-east-1:${AWS_ACCOUNT_ID}:parameter/cruddur/backend-flask/CONNECTION_URL" },
        {"name": "ROLLBAR_ACCESS_TOKEN" , "valueFrom": "arn:aws:ssm:us-east-1:${AWS_ACCOUNT_ID}:parameter/cruddur/backend-flask/ROLLBAR_ACCESS_TOKEN" },
        {"name": "OTEL_EXPORTER_OTLP_HEADERS" , "valueFrom": "arn:aws:ssm:us-east-1:${AWS_ACCOUNT_ID}:parameter/cruddur/backend-flask/OTEL_EXPORTER_OTLP_HEADERS" }
      ]
    }
  ]
}
```

#### Register Task Defintion

```sh
aws ecs register-task-definition --cli-input-json file://aws/task-definitions/backend-flask.json
```

#### Set default VPC, Subnets, and Service SG

* Default VPC ID
```sh
export DEFAULT_VPC_ID=$(aws ec2 describe-vpcs \
--filters "Name=isDefault, Values=true" \
--query "Vpcs[0].VpcId" \
--output text)
echo $DEFAULT_VPC_ID
```

#### Default Subnet IDs
```sh
export DEFAULT_SUBNET_IDS=$(aws ec2 describe-subnets  \
 --filters Name=vpc-id,Values=$DEFAULT_VPC_ID \
 --query 'Subnets[*].SubnetId' \
 --output json | jq -r 'join(",")')
echo $DEFAULT_SUBNET_IDS
```

#### Create service SG

```sh
export CRUD_SERVICE_SG=$(aws ec2 create-security-group \
  --group-name "crud-srv-sg" \
  --description "Security group for Cruddur services on ECS" \
  --vpc-id $DEFAULT_VPC_ID \
  --query "GroupId" --output text)
echo $CRUD_SERVICE_SG
```


#### Update RDS SG to allow access for the service security group
```sh
aws ec2 authorize-security-group-ingress \
  --group-id $DB_SG_ID \
  --protocol tcp \
  --port 5432 \
  --source-group $CRUD_SERVICE_SG \
  --tag-specifications 'ResourceType=security-group,Tags=[{Key=Name,Value=BACKENDFLASK}]'`
```

#### Create the backend service
  
 -  Create service-backend-flask.json inside aws/json
```sh
{
  "cluster": "cruddur",
  "launchType": "FARGATE",
  "desiredCount": 1,
  "enableECSManagedTags": true,
  "enableExecuteCommand": true,
  "networkConfiguration": {
    "awsvpcConfiguration": {
      "assignPublicIp": "ENABLED",
      "securityGroups": [
        "$CRUD_SERVICE_SG"
      ],
      "subnets": [
        "$DEFAULT_SUBNET_IDS"
      ]
    }
  },
  "serviceConnectConfiguration": {
    "enabled": true,
    "namespace": "cruddur",
    "services": [
      {
        "portName": "backend-flask",
        "discoveryName": "backend-flask",
        "clientAliases": [{"port": 4567}]
      }
    ]
  },
  "propagateTags": "SERVICE",
  "serviceName": "backend-flask",
  "taskDefinition": "backend-flask"
}
```

#### Create the backend service
  
 -  Create service-front-react-js.json inside aws/json

```sh
{
    "cluster": "cruddur",
    "launchType": "FARGATE",
    "desiredCount": 1,
    "enableECSManagedTags": true,
    "enableExecuteCommand": true,
    "loadBalancers": [
        {
            "targetGroupArn": "arn:aws:elasticloadbalancing:eu-west-3:6****3****34****:targetgroup/cruddur-frontend-react-js/eb93a****294e0",
            "containerName": "frontend-react-js",
            "containerPort": 3000
        }
      ],
    "networkConfiguration": {
      "awsvpcConfiguration": {
        "assignPublicIp": "ENABLED",
        "securityGroups": [
          "sg-00****99d385b"
        ],
        "subnets": [
            "subnet-0fb08d49d1c6771f9",
            "subnet-097ee*****1d1283",
            "subnet-0fd9****6183e3d"
        ]
      }
    },
    "propagateTags": "SERVICE",
    "serviceName": "frontend-react-js",
    "taskDefinition": "frontend-react-js",
    "serviceConnectConfiguration": {
      "enabled": true,
      "namespace": "cruddur",
      "services": [
        {
          "portName": "frontend-react-js",
          "discoveryName": "frontend-react-js",
          "clientAliases": [{"port": 3000}]
        }
      ]
    }
  }
```

 #### Create Services
  
  ```.sh
  aws ecs create-service --cli-input-json file://aws/json/service-backend-flask.json
  ```
  ```.sh
  aws ecs create-service --cli-input-json file://aws/json/service-frontend-react-js.json
  ```
  
  <img width="1679" alt="Screenshot 2023-05-07 at 01 02 48" src="https://user-images.githubusercontent.com/11331502/236649682-db473f55-0250-4b9a-b567-0030023bd23e.png">


## Provision and configure Application Load Balancer along with target groups

* Enter the ALB name  
* choose the VPC (default in my case) and subnets  
* For security group, Create a new SG (cruddur-alb-sg)  
  - Allow inbound rules on ports (3000,4567,80,443) from Anywhere *"Temporarily"*  
* Choose the created SG
* For listener:
  - Enter *4567* for port number and forward to the backend target group
  - Create a new listener and enter *3000* for port number and forward to the frontend target group

* Update the *Service security group* to allow traffic on port 4567 from the load balancer  


- Traget groups 
<img width="1680" alt="Screenshot 2023-05-07 at 01 21 44" src="https://user-images.githubusercontent.com/11331502/236650101-be4f1430-f343-49fe-bb2c-8e69794630d6.png">

- LB

<img width="1680" alt="Screenshot 2023-05-07 at 01 22 28" src="https://user-images.githubusercontent.com/11331502/236650124-3283fac3-e4e2-4fc8-9736-d51edb87653f.png">

### Add the load balancer into the backend service.json
* Add the below ALB definition into the `aws/json/service-backend-flask.json`  
```json
"loadBalancers": [
    {
        "targetGroupArn": "${ALB TG ARN}",
        "containerName": "backend-flask",
        "containerPort": 4567
    }
  ],
```
---




---
## Create ECR repo and push image for fronted-react-js

#### Create a new `Dockerfile.prod` inside `frontend-react-js` folder 

 
```Dockerfile
# Base Image ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
FROM node:16.18 AS build

ARG REACT_APP_BACKEND_URL
ARG REACT_APP_AWS_PROJECT_REGION
ARG REACT_APP_AWS_COGNITO_REGION
ARG REACT_APP_AWS_USER_POOLS_ID
ARG REACT_APP_CLIENT_ID

ENV REACT_APP_BACKEND_URL=$REACT_APP_BACKEND_URL
ENV REACT_APP_AWS_PROJECT_REGION=$REACT_APP_AWS_PROJECT_REGION
ENV REACT_APP_AWS_COGNITO_REGION=$REACT_APP_AWS_COGNITO_REGION
ENV REACT_APP_AWS_USER_POOLS_ID=$REACT_APP_AWS_USER_POOLS_ID
ENV REACT_APP_CLIENT_ID=$REACT_APP_CLIENT_ID

COPY . ./frontend-react-js
WORKDIR /frontend-react-js
RUN npm install
RUN npm run build

# New Base Image ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
FROM nginx:1.23.3-alpine

# --from build is coming from the Base Image
COPY --from=build /frontend-react-js/build /usr/share/nginx/html
COPY --from=build /frontend-react-js/nginx.conf /etc/nginx/nginx.conf

EXPOSE 3000
```

####  Create `nginx.conf` inside `frontend-react-js` folder  

```sh
# Set the worker processes
worker_processes 1;

# Set the events module
events {
  worker_connections 1024;
}

# Set the http module
http {
  # Set the MIME types
  include /etc/nginx/mime.types;
  default_type application/octet-stream;

  # Set the log format
  log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                    '$status $body_bytes_sent "$http_referer" '
                    '"$http_user_agent" "$http_x_forwarded_for"';

  # Set the access log
  access_log  /var/log/nginx/access.log main;

  # Set the error log
  error_log /var/log/nginx/error.log;

  # Set the server section
  server {
    # Set the listen port
    listen 3000;

    # Set the root directory for the app
    root /usr/share/nginx/html;

    # Set the default file to serve
    index index.html;

    location / {
        # First attempt to serve request as file, then
        # as directory, then fall back to redirecting to index.html
        try_files $uri $uri/ $uri.html /index.html;
    }

    # Set the error page
    error_page  404 /404.html;
    location = /404.html {
      internal;
    }

    # Set the error page for 500 errors
    error_page  500 502 503 504  /50x.html;
    location = /50x.html {
      internal;
    }
  }
}
```


#### Build   
  
```sh
docker build \
--build-arg REACT_APP_BACKEND_URL="http://${LOADBALANCER-DNS-NAME}:4567" \
--build-arg REACT_APP_AWS_PROJECT_REGION="$AWS_DEFAULT_REGION" \
--build-arg REACT_APP_AWS_COGNITO_REGION="$AWS_DEFAULT_REGION" \
--build-arg REACT_APP_AWS_USER_POOLS_ID="${REACT_APP_AWS_USER_POOLS_ID}" \
--build-arg REACT_APP_CLIENT_ID="${REACT_APP_CLIENT_ID}" \
-t frontend-react-js \
-f Dockerfile.prod \
.
```

#### Tag   
```sh
docker tag backend-flask:latest $ECR_BACKEND_FLASK_URL:latest
```
#### Push Image  
```sh
docker push $ECR_BACKEND_FLASK_URL:latest
```

#### Create Frontend task definition
* In the `aws/task-definitions` folder, Create a new file `frontend-react-js.json`
```json
{
  "family": "frontend-react-js",
  "executionRoleArn": "arn:aws:iam::${AWS_ACCOUNT_ID}:role/CruddurServiceExecutionRole",
  "taskRoleArn": "arn:aws:iam::${AWS_ACCOUNT_ID}:role/CruddurTaskRole",
  "networkMode": "awsvpc",
  "cpu": "256",
  "memory": "512",
  "requiresCompatibilities": [ 
    "FARGATE" 
  ],
  "containerDefinitions": [
    {
      "name": "frontend-react-js",
      "image": "FRONTEND_IMAGE_URL",
      "essential": true,
      "healthCheck": {
        "command": [
          "CMD-SHELL",
          "curl -f http://localhost:3000 || exit 1"
        ],
        "interval": 30,
        "timeout": 5,
        "retries": 3
      },
      "portMappings": [
        {
          "name": "frontend-react-js",
          "containerPort": 3000,
          "protocol": "tcp", 
          "appProtocol": "http"
        }
      ],

      "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
            "awslogs-group": "cruddur",
            "awslogs-region": "$AWS_DEFAULT_REGION",
            "awslogs-stream-prefix": "frontend-react-js"
        }
      }
    }
  ]
}
```

####  Register Task Defintion
```sh
aws ecs register-task-definition --cli-input-json file://aws/task-definitions/frontend-react-js.json
```

#### Create the frontend service

* Create `service-frontend-react-js.json` inside `aws/json`
```json
{
  "cluster": "cruddur",
  "launchType": "FARGATE",
  "desiredCount": 1,
  "enableECSManagedTags": true,
  "enableExecuteCommand": true,
  "loadBalancers": [
    {
        "targetGroupArn": "${ALB TG ARN}",
        "containerName": "frontend-react-js",
        "containerPort": 3000
    }
  ],
  "networkConfiguration": {
    "awsvpcConfiguration": {
      "assignPublicIp": "ENABLED",
      "securityGroups": [
        "$CRUD_SERVICE_SG"
      ],
      "subnets": [
        "$DEFAULT_SUBNET_IDS"
      ]
    }
  },
  "propagateTags": "SERVICE",
  "serviceName": "frontend-react-js",
  "taskDefinition": "frontend-react-js",
  "serviceConnectConfiguration": {
    "enabled": true,
    "namespace": "cruddur",
    "services": [
      {
        "portName": "frontend-react-js",
        "discoveryName": "frontend-react-js",
        "clientAliases": [{"port": 3000}]
      }
    ]
  }
}
```

#### Deploy the service

```sh
aws ecs create-service --cli-input-json file://aws/json/service-frontend-react-js.json
```

<img width="1680" alt="Screenshot 2023-05-07 at 01 31 06" src="https://user-images.githubusercontent.com/11331502/236650374-57f5b402-ebfc-41f3-ba9d-1d1b762d64f9.png">


---
## Manage domain useing Route53 via hosted zone

* My website domain will be ***shopili.net***  
* Create a new Route53 hosted zone ( Public with our domain name)  

<img width="1680" alt="Screenshot 2023-05-07 at 01 33 09" src="https://user-images.githubusercontent.com/11331502/236650421-879df6e7-db56-4928-969e-3f8c07071749.png">


### Create an SSL certificate via ACM

* From ***AWS Certificate Manager*** Service, Request a new public certificate.  
* In the name entered domain name `"cruddur.space"` and add another one `"*.cruddur.space"`.  
* From the certificate page, Click **"Create records in Route 53"** and select both Domains you entered perviously.  


<img width="1680" alt="Screenshot 2023-05-07 at 01 34 34" src="https://user-images.githubusercontent.com/11331502/236650483-e906b362-452c-49dd-8a93-df75ac5b2667.png">

### Update ALB Listeners

* From Load Balancer page, Add 2 new listeners  
  - First listener from port *80* > Action > redirects to HTTPS 443  
  - Second Listener from port *443* > Action > forwards to *Frontend-react-js* target group  
  - Choose the certificate we just created previously  
  - Remove other listeners  
  - Update the *443* Listener rules to forward *api.cruddur.space* to *backend-flask* target group  
  
<img width="1677" alt="Screenshot 2023-05-07 at 01 36 33" src="https://user-images.githubusercontent.com/11331502/236650511-e97bbdfd-40f8-4987-ae67-89e7903fdc85.png">


### Add ALB records in Route53 Hosted zone
* From Route53 Hosted zone page, Create 2 new records  
  - First record for naked domain to point to *frontend-react-js* ( Choose the *Alias* and region and load balancer)  
  - Second record for api subdomain to point to *the backend-flask*


### Test applications 

* Now let's check the backend service traffic redirection from health check endpoint!  

--- BACKEND 
<img width="1675" alt="Screenshot 2023-05-07 at 01 38 14 1" src="https://user-images.githubusercontent.com/11331502/236650554-b8b94cb9-043e-40ed-92df-3115a71e19cc.png">

--- FRONTEND
<img width="1670" alt="Screenshot 2023-05-07 at 01 39 04" src="https://user-images.githubusercontent.com/11331502/236650570-2d301673-af5f-4ba9-8d1e-2bce89b7f421.png">


### Configure CORS to only permit traffic from our domain

* Update backend & frontend URLs under *environment* in `service-backend-flask.json`  


```json
{"name": "FRONTEND_URL", "value": "https://cruddur.space"},
{"name": "BACKEND_URL", "value": "https://api.cruddur.space"},
```





## 	Secure Flask by not running in debug mode

  ''Enable debugging in development only''  
* Update the backend-flask `Dockerfile` to use `--debug` flag in flask run command  
* To prevent debug mode from production environment:  
  - use `"--no-debugger"` and `"--no-reload` flags in the `Dockerfile.prod` CMD   
```Dockerfile
CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0", "--port=4567", "--no-debug","--no-debugger","--no-reload"]
```
 


---
## Implement Refresh Token for Amazon Cognito

In order to resolve the expiring token problem, we have to use a method to refresh the current user session.  

* Below will create a function to get the access token and update the authentication check to set the access token into local storage  
* Update the `frontend-react-js/src/lib/CheckAuth.js`  
```js
import { Auth } from 'aws-amplify';
import { resolvePath } from 'react-router-dom';

export async function getAccessToken(){
  Auth.currentSession()
  .then((cognito_user_session) => {
    const access_token = cognito_user_session.accessToken.jwtToken
    localStorage.setItem("access_token", access_token)
  })
  .catch((err) => console.log(err));
}

export async function checkAuth(setUser){
  Auth.currentAuthenticatedUser({
    // Optional, By default is false. 
    // If set to true, this call will send a 
    // request to Cognito to get the latest user data
    bypassCache: false 
  })
  .then((cognito_user) => {
    console.log('cognito_user',cognito_user);
    setUser({
      display_name: cognito_user.attributes.name,
      handle: cognito_user.attributes.preferred_username
    })
    return Auth.currentSession()
  }).then((cognito_user_session) => {
      console.log('cognito_user_session',cognito_user_session);
      localStorage.setItem("access_token", cognito_user_session.accessToken.jwtToken)
  })
  .catch((err) => console.log(err));
};
```
---
## Apply the update to the authentication required pages
* Update `MessageForm.js`, `HomeFeedPage.js`, `MessageGroupPage.js`, `MessageGroupsPage.js`  
* Import function  
```js
import {getAccessToken} from '../lib/CheckAuth';
```
* Get access token  
```js
await getAccessToken()
      const access_token = localStorage.getItem("access_token")
```
Replace the ***Authorization header*** line  
```js
'Authorization': `Bearer ${access_token}`,
```

 

---
## Refactor bin directory to be top level
First I created the required scripts, then restructured the **bin** directory containing all scripts  
![Screenshot 2023-05-07 at 01 41 01](https://user-images.githubusercontent.com/11331502/236650631-adff9bb5-c63f-4273-ab73-c5fe8ee0b39e.png)



---
## Configure task definitions to contain X-RAY
* In backend & frontend task definitions add the follwing defintion for X-RAY  
`aws/task-definitions/backend-flask.json` & `aws/task-definitions/frontend-react-js.json` under ***"containerDefinitions"***  
```json
{
      "name": "xray",
      "image": "public.ecr.aws/xray/aws-xray-daemon" ,
      "essential": true,
      "user": "1337",
      "portMappings": [
        {
          "name": "xray",
          "containerPort": 2000,
          "protocol": "udp"
        }
      ]
    },
```

 <img width="1680" alt="Screenshot 2023-05-07 at 01 45 38" src="https://user-images.githubusercontent.com/11331502/236650752-28a625c7-2b23-4b63-873d-cf0e6fa269cb.png">

 
---
## Generate .env files using ruby for docker
  ''After storing variables in a `.env` file, the ***docker run*** command didn't pass some variables correctly''  

  Following along with Andrew, Created a ruby script to generate ***.env*** files using ***erb templates***  

* Create a folder `erb` for the erb files in the project root folder  
* Create `.erb` files with the variables to generate from  

`backend-flask.env.erb`   
```ruby
AWS_ENDPOINT_URL=http://dynamodb-local:8000
CONNECTION_URL=postgresql://postgres:password@db:5432/cruddur
FRONTEND_URL=https://3000-<%= ENV['GITPOD_WORKSPACE_ID'] %>.<%= ENV['GITPOD_WORKSPACE_CLUSTER_HOST'] %>
BACKEND_URL=https://4567-<%= ENV['GITPOD_WORKSPACE_ID'] %>.<%= ENV['GITPOD_WORKSPACE_CLUSTER_HOST'] %>
OTEL_SERVICE_NAME=backend-flask
OTEL_EXPORTER_OTLP_ENDPOINT=https://api.honeycomb.io
OTEL_EXPORTER_OTLP_HEADERS=x-honeycomb-team=<%= ENV['HONEYCOMB_API_KEY'] %>
AWS_XRAY_URL=*4567-<%= ENV['GITPOD_WORKSPACE_ID'] %>.<%= ENV['GITPOD_WORKSPACE_CLUSTER_HOST'] %>*
AWS_XRAY_DAEMON_ADDRESS=xray-daemon:2000
AWS_DEFAULT_REGION=<%= ENV['AWS_DEFAULT_REGION'] %>
AWS_ACCESS_KEY_ID=<%= ENV['AWS_ACCESS_KEY_ID'] %>
AWS_SECRET_ACCESS_KEY=<%= ENV['AWS_SECRET_ACCESS_KEY'] %>
ROLLBAR_ACCESS_TOKEN=<%= ENV['ROLLBAR_ACCESS_TOKEN'] %>
AWS_COGNITO_USER_POOL_ID=<%= ENV['AWS_COGNITO_USER_POOL_ID'] %>
AWS_COGNITO_USER_POOL_CLIENT_ID=<%= ENV['AWS_COGNITO_USER_POOL_CLIENT_ID'] %>
```

`frontend-react-js.env.erb`  
```ruby
REACT_APP_BACKEND_URL=https://4567-<%= ENV['GITPOD_WORKSPACE_ID'] %>.<%= ENV['GITPOD_WORKSPACE_CLUSTER_HOST'] %>
REACT_APP_AWS_PROJECT_REGION=<%= ENV['AWS_DEFAULT_REGION'] %>
REACT_APP_AWS_COGNITO_REGION=<%= ENV['AWS_DEFAULT_REGION'] %>
REACT_APP_AWS_USER_POOLS_ID=<%= ENV['REACT_APP_AWS_USER_POOLS_ID'] %>
REACT_APP_CLIENT_ID=<%= ENV['REACT_APP_CLIENT_ID'] %>
```
* Create the ruby scripts  

`bin/backend/generate-env`
```ruby
#!/usr/bin/env ruby

require 'erb'

template = File.read 'erb/backend-flask.env.erb'
content = ERB.new(template).result(binding)
filename = "backend-flask.env"
File.write(filename, content)
```

`bin/frontend/generate-env`
```ruby
#!/usr/bin/env ruby

require 'erb'

template = File.read 'erb/frontend-react-js.env.erb'
content = ERB.new(template).result(binding)
filename = "frontend-react-js.env"
File.write(filename, content)
```

* To generate the env files anytime, just run the `generate-env` scripts  

  ''Now after generating the .env files, we don't want to push these secrets into our github repo''  
* Add `*.env` into `.gitignore` file  

* add the below 2 commands in `.gitpod.yml` to run ***"generate-env"*** scripts at workspace launch  
```yml
source "$THEIA_WORKSPACE_ROOT/bin/backend/generate-env"
source "$THEIA_WORKSPACE_ROOT/bin/frontend/generate-env"
```
![Screenshot 2023-05-07 at 01 49 33](https://user-images.githubusercontent.com/11331502/236650924-73bbf75c-ada3-4456-81f6-0a67e6804471.png)



---
## Change Docker Compose to explicitly use env files and user-defined network
### To use the variables from our generated .env files  
* In `docker-compose.yml` file, replace the ***environment*** definition and it's contents in frontend/backend services with the below:  
```yml
env_file:
      - backend-flask.env
```
```yml
env_file:
      - frontend-react-js.env
```
### To use our defined network  
* Replace the ***networks*** section with the below:  
```yml
networks:
  cruddur-net:
    driver: bridge
    name: cruddur-net
```
* Add the below network definition under every service  
```yml
networks:
      - cruddur-net
```
![Screenshot 2023-05-07 at 01 51 11](https://user-images.githubusercontent.com/11331502/236650870-5fd927bd-3202-4993-b1c7-18c79a2e644d.png)



  
  
 
