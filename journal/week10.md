# Week 10 — CloudFormation Part 1



## Prerequisites

-  Install the linyter for cloudformation templates validation.  

```sh
pip install cfn-lint
```

- Install **"cfn-guard"** using cargo for evaluating our cloudformation templates compare to the best practices.  

```sh
cargo install cfn-guard
```

* Create Deploy file.  
```sh
#! /usr/bin/env bash
set -e # stop the execution of the script if it fails

CFN_PATH="/workspace/aws-bootcamp-cruddur-2023/aws/cfn/template.yaml"
echo $CFN_PATH

cfn-lint $CFN_PATH

aws cloudformation deploy \
  --stack-name "my-cluster" \
  --s3-bucket "cfn-artifacts-2" \
  --template-file "$CFN_PATH" \
  --no-execute-changeset \
  --capabilities CAPABILITY_NAMED_IAM
```

* Create tempalte.yaml 

```yml
AWSTemplateFormatVersion: 2010-09-09
Description: |
  Setup ECS Cluster
Resources:
  ECSCluster: #LogicalName
    Type: 'AWS::ECS::Cluster'
    Properties:
      ClusterName: MyCluster1
      CapacityProviders:
        - FARGATE
#Parameters:
#Mappings:
#Outputs:
#Metadata:
```

* Create the artifacts S3 bucket and add it to the deploy scripts  

Set it as environment variable  
```sh
export CFN_BUCKET="cfn-artifacts-2"
gp env CFN_BUCKET="cfn-artifacts-2"
```

![Screenshot 2023-06-11 at 13 21 13](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/e263d80c-59e8-47e0-8502-8f41d961d251)


* Add insrall CFN command `.gitpod.yml` to be included in workspace initialization 

```sh
  - name: cfn
    before: |
      pip install cfn-lint
      cargo install cfn-guard
```
 ![Screenshot 2023-06-11 at 12 32 26](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/65da5375-74d1-4068-86a3-5d2ab0e44be9)

[Commit](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/commit/b807c270d3f371870c76aae286182d5cd61bfab1)


---

## CFN Networking Layer

- "aws/cfn/networking/template.yaml"
- "aws/cfn/networking/config.toml"
- "bin/cfn/networking"
- 
[Commit](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/commit/e96ff0e70e1f09f94fe9ea08a39dd64696118a51)

![Screenshot 2023-06-15 at 20 15 13](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/cf4e518d-de09-4711-b688-7ba8602a2c17)

---

## CFN Cluster 
- Delete manually all created services(ECS cluster, services, tasks, namespaces, ALB + listeners, target groups, etc...)
- Create cluster CFN template "aws/cfn/cluster/template.yaml"
- Create configuration TOML file "aws/cfn/cluster/config.toml"
- Create CFN deploy bash script "bin/cfn/cluster"
 
[Commit](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/commit/89ad2fbb710cf94c32e19f898f0834519df3b1c0)
![Screenshot 2023-06-15 at 20 15 13](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/44239195-de73-4abb-a84b-1e6eefac3d80)

---

## Implement CFN Database Layer (RDS)
- Create RDS CFN template "aws/cfn/db/template.yaml"
- Create configuration TOML file "aws/cfn/db/config.toml"
- Create CFN deploy bash script "bin/cfn/db"
[Commit](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/commit/89ad2fbb710cf94c32e19f898f0834519df3b1c0)
![Screenshot 2023-06-16 at 18 21 30](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/ba010971-6fff-452a-af96-b27728b18355)


---
## Implement CFN Service Layer for Backend

- Delete the manually created services from AWS console (IAM excution & task roles, etc...)
- Create Service CFN template "aws/cfn/service/template.yaml"
- Create configuration TOML file "aws/cfn/service/config.toml"
- Create CFN deploy bash script "bin/cfn/service"
[Commit](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/commit/89ad2fbb710cf94c32e19f898f0834519df3b1c0)
![Screenshot 2023-06-16 at 18 08 25](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/c3260f43-6b01-4034-8ac1-80b5e4295f66)
![Screenshot 2023-06-16 at 18 07 55](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/5ac6905e-d6a6-4f0d-adda-250e3bd2a6b2)

Health check : 
![Screenshot 2023-06-16 at 18 22 20](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/05ec8a57-691f-49b0-8e64-820c5c9709d7)


## Implement DynamoDB using SAM 
***Install SAM CLI**
```sh
cd /workspace
wget https://github.com/aws/aws-sam-cli/releases/latest/download/aws-sam-cli-linux-x86_64.zip
unzip aws-sam-cli-linux-x86_64.zip -d sam-installation
sudo ./sam-installation/install
cd $THEIA_WORKSPACE_ROOT
```
***CFN for DDB***
- Create DDB CFN template "ddb/template.yaml"
- Create configuration TOML file "ddb/config.toml"
- Create the lambda function will be deployed "/ddb/cruddur-messaging-stream/lambda_function.py"
***manage DDB*** 
Create DDB scripts
- "ddb/build"
- "ddb/package"
- "ddb/deploy"
[Commit](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/commit/e8c42414c5a7bc3e076da212f0523f661d29961f)
![Screenshot 2023-06-23 at 18 38 05](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/29e88775-007b-49ca-a7c7-b730189ad9cc)

---

## Implement CI/CD CFN
- Create CICD CFN template "aws/cfn/cicd/template.yaml"
- Create configuration TOML file "aws/cfn/cicd/config.toml"
- Create CFN deploy bash script "bin/cfn/cicd"
- Create Codebuild nested stack "aws/cfn/cicd/nested/codebuild.yaml"
- Create S3 bucket manually to be the artifacts store. In my case "codepipeline-cruddur-artifacts-space"
[Commit](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/commit/b014e1a874c1fe83f2b65ad5e7cde4ba960f850b)
![Screenshot 2023-06-23 at 19 37 33](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/6e853489-9705-44cf-a07c-c92d18ef600d)


--- 
## Implement CFN Static Website Hosting for Frontend

- Create Frontend CFN template "aws/cfn/frontend/template.yaml"
- Create configuration TOML file "aws/cfn/frontend/config.toml"
- Create CFN deploy bash script "bin/cfn/frontend"
[Commit](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/commit/0f4db80c78819148a6555fd74042d44500fd62d9)
<img width="448" alt="Screenshot 2023-06-23 at 22 14 59" src="https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/f1839b73-d5cd-429a-bd7f-7e07de85d143">



[Diagramme](https://viewer.diagrams.net/index.html?tags=%7B%7D&highlight=0000ff&edit=_blank&layers=1&nav=1&title=Diagramme%20sans%20nom.drawio#Uhttps%3A%2F%2Fraw.githubusercontent.com%2FMossaaba%2Faws-bootcamp%2Fmain%2FDiagramme%2520sans%2520nom.drawio)
