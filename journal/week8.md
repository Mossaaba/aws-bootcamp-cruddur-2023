# Week 8 — Serverless Image Processing

# Implement CDK Stack

### New Directory

Lets contain our cdk pipeline in a new top level directory called:
```sh
cd /workspace/aws-bootcamp-cruddur-2023
mkdir thumbing-serverless-cdk
```
### Install CDK globally
This will enable us to use our cdk command anywhere in the project workspace
```sh
npm install aws-cdk -g
```
We'll add the the install to our gitpod task file
```sh
  - name: cdk
    before: |
      npm install aws-cdk-lib -g
```
### Initialize a new project
We'll initialize a new cdk project within the folder we created:
```sh
cdk init app --language typescript
```
### Bootstrap : 
Run cdk bootstrap to provision resources for the AWS CDK before we deploy out AWS CDK Stack : 
```sh
cdk bootstrap "aws://$AWS_ACCOUNT_ID/$AWS_DEFAULT_REGION"
```
![Screenshot 2023-05-11 at 18 54 22](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/761977d1-021d-4df1-984a-a0c1ef7b2cf5)

### Adding IaC for the PipeLine writtten in typescript
Install the following dependencies.

```sh 
npm install aws-cdk-lib
npm install sharp
npm install dotenv
```


### Create a Bash script for upload and delete object in S3

Uploads

```sh
#! /usr/bin/bash

ABS_PATH=$(readlink -f "$0")
SERVERLESS_PATH=$(dirname $ABS_PATH)
DATA_FILE_PATH="$SERVERLESS_PATH/files/data.jpg"
aws s3 cp "$DATA_FILE_PATH" "s3://assets.$DOMAIN_NAME/avatars/original/data.jpg"
```
delete
```sh
#! /usr/bin/bash

ABS_PATH=$(readlink -f "$0")
SERVERLESS_PATH=$(dirname $ABS_PATH)
DATA_FILE_PATH="$SERVERLESS_PATH/files/data.jpg"
aws s3 rm "s3://assets.$DOMAIN_NAME/avatars/original/data.jpg"
aws s3 rm "s3://assets.$DOMAIN_NAME/avatars/processed/data.jpg"
```

### Add an S3 Bucket

Add the following code to your `thumbing-serverless-cdk-stack.ts`

```ts
import * as s3 from 'aws-cdk-lib/aws-s3';
const bucket_name = process.env.THUMBBING_BUCKET_NAME;
const bucket = new s3.Bucket(this, bucket_name, {
  removalPolicy: cdk.RemovalPolicy.DESTROY,
});
```

![Screenshot 2023-05-11 at 18 50 23](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/2d82de67-7fc7-4853-85f8-31af06582b18)

![Screenshot 2023-05-11 at 18 41 33](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/c0f43dfe-1252-4e82-a4f5-b699d93fcba5)

![Screenshot 2023-05-11 at 18 56 26](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/8cb1e2be-52c5-4160-b386-99ab91cda2a9)


### Create Lambda function for processing uploaded images : 

```sh
cd aws/Lambdas/process-images
npm init -y
npm install sharp @aws-sdk/client-s3
```

 ![Screenshot 2023-05-12 at 19 06 08](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/bf90a515-cf0f-4cb9-944d-eff0db98712c)
![Screenshot 2023-05-12 at 19 07 33](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/c7fe95c0-34c4-4aa3-9e9f-3aa144d65abd)
<img width="1680" alt="Screenshot 2023-05-14 at 18 25 27" src="https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/343488e7-c955-48c1-b22d-67fdf1ca38af">
<img width="1680" alt="Screenshot 2023-05-14 at 18 39 47" src="https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/a66b06ea-773c-418a-ab2d-6e91510dc7f3">


### CDK commande : 


***Install : Install SDK globally 
```sh
npm install aws-cdk -g
```
***CDK synth : Synthesize the AWS CloudFormation stack(s) that represent your infrastructure as code.
```sh
cdk synth
```
***CDK deploy: Deploy your stack to the AWS cloud.
```sh
cdk deploy
```
***CDK ls: List your available stacks in AWS CloudFormation.
```sh
cdk ls
```


## Serve Avatars via CloudFront 


Amazon CloudFront is a web service that speeds up distribution of your static and dynamic web content, such as .html, .css, .js, and image files, to your users. CloudFront delivers your content through a worldwide network of data centers called edge locations. When a user requests content that you're serving with CloudFront, the request is routed to the edge location that provides the lowest latency (time delay), so that content is delivered with the best possible performance. 

If the content is already in the edge location with the lowest latency, CloudFront delivers it immediately. If the content is not in that edge location, CloudFront retrieves it from an origin that you've defined—such as an Amazon S3 bucket, a MediaPackage channel, or an HTTP server (for example, a web server) that you have identified as the source for the definitive version of your content.

**Create a distribution : 
From the AWS CloudFront console, Create new distribution: 

- Origin domain: ASSETS_BUCKET_NAME.
- Origin access: Origin access control settings (recommended) and choose your assets bucket name from the drop-down menu "If not, create control setting".
- Viewer protocol policy: Viewer protocol policy.
- Cache policy:  CachingOptimized 
- Origin request policy: CORS
- CustomOrigin 
- Response headers policy: SimpleCORS.
- Alternate domain name (CNAME): Add item then enter assets.<your_domain_name>.
- Custom SSL certificate: select the SSL certificate requested from ACM.

![Screenshot 2023-06-05 at 18 23 58](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/cc7c2634-727f-474f-aa37-152b0ab65261)


**Create Route53 record in hostedzone : 

Record name: assets.<domain_name>.
Select Alias and Route traffic to Alias to CloudFront distribution then choose the created Cloudfront dist.
![Screenshot 2023-06-05 at 18 36 46](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/ac8e4e15-17d1-45f6-8a2c-a47b1f0fba38)

**Test :
![Screenshot 2023-06-05 at 18 26 07](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/50a01858-a4e0-49fd-9886-8bab443b9083)




## Implement Users Profile Page : 

- Create new db query show.sql in backend-flask/bd/sql/users to get user data shown in the profile page.
- Update "users_activities.py" to return the users from db query.
- Split the profile heading into a separate file and import it in UserFeedPage.js.
- Create ProfileHeading.js & ProfileHeading.css components.
- Update UserFeedPage.js 
- Create EditProfileButton.js & EditProfileButton.css components.
- Update activity feed heading in ActivityFeed.js, HomeFeedPage.js, NotificationsFeedPage.js.
[Commit](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/commit/54e0f7473481160214679d38778ca0b0956c91ac)  

![Screenshot 2023-06-05 at 19 45 22](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/b3446360-8401-4556-b322-6ea378f3ba64)


[Commit](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/commit/54e0f7473481160214679d38778ca0b0956c91ac)  

## Implement Backend Migrations
[Commit](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/commit/92281075cb771d6409b86ae74394394f02cfcce8) 

![Screenshot 2023-06-05 at 23 03 15](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/1eb26ea0-ffdd-4313-a597-57f107ea9f6c)

## Implement Users Profile Form
[Commit](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/commit/92281075cb771d6409b86ae74394394f02cfcce8) 

![Screenshot 2023-06-05 at 23 00 35](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/3f9e07a2-d722-4b20-aee8-3dc78f29c0be)
![Screenshot 2023-06-05 at 23 00 57](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/c36d0308-dfc5-495e-b631-c61f4ec9d441)

 


## Upload Avatar Implementation

install aws-sdk for client s3 
```sh
npm i @aws-sdk/client-s3 --save
```

Use Ruy to generate URL
1. init the project ```sh bundel init ```
2. Create GembFile
3. Create function : 
**function.rb :
```ruby
require 'aws-sdk-s3'
require 'json'

def handler(event:, context:)
  puts event
  s3 = Aws::S3::Resource.new
  bucket_name = ENV["UPLOADS_BUCKET_NAME"]
  object_key = 'mock.jpg'

  obj = s3.bucket(bucket_name).object(object_key)
  url = obj.presigned_url(:put, expires_in: 60 * 5)
  url # this is the data that will be returned
  body = {url: url}.to_json
  { statusCode: 200, body: body }
end

puts handler(
  event: {},
  context: {}
)
```
**Generate presigned URL :
![Screenshot 2023-06-07 at 21 04 50](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/8556bf7d-26f7-4bab-b070-316b20a13661)
![Screenshot 2023-06-07 at 21 05 58](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/d3187481-abcd-4591-8335-a844a47be501)

**create CruddurAvatarUpload function under AWS LAMBDA :

**Policy && ApiGatewayAuthorizatoin : 

## Render Avatars in App via CloudFront
![Screenshot 2023-06-09 at 18 56 26](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/334a7fa3-6b91-4136-b8a2-e3faca145953)

[Commit][https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/commit/92281075cb771d6409b86ae74394394f02cfcce8]
