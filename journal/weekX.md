## Week X


## Building the frontend static

- Create "static-build" script "bin/frontend/static-build"
- After running the "static-build" script, I had to correct some warnings in the code.
- Build the project, then zip it
```sh
zip -r build.zip build/
```
- Upload the folder contents into S3 bucket.
<img width="1680" alt="Screenshot 2023-06-24 at 11 17 14" src="https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/23f8844b-d787-4b65-979b-8279ec6f268e">

 [Commit](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/commit/b43383d31406be2e132b6216df8fd0b8259c379e)

---
## Automating frontend sync process

### Prepare environment

* Install aws_s3_website_sync  
```sh
cd /workspace
gem install aws_s3_website_sync dotenv
```
* Create ***sync"*** script

* Update ***"bin/frontend/generate-env"*** with ***sync*** to generate sync.env
```ruby
#!/usr/bin/env ruby

require 'erb'

template = File.read 'erb/frontend-react-js.env.erb'
content = ERB.new(template).result(binding)
filename = "frontend-react-js.env"
File.write(filename, content)

template = File.read 'erb/sync.env.erb'
content = ERB.new(template).result(binding)
filename = "sync.env"

File.write(filename, content)
```
* Create ***"/erb/sync.env.erb"*** to keep environment variables

### Test the change 

* Now let's update the 'About' in the footer of the home page by adding ':)' and check the sync process :   
- Make the change.
- run build command

```sh
 ./bin/frontend/static-build 
 ```
- run sync command
```sh
 ./bin/frontend/sync 
```
<img width="458" alt="Screenshot 2023-06-23 at 23 24 53" src="https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/assets/11331502/71c41b09-2109-407b-9a27-8da947c7f192">

 [Commit](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/commit/8031b23e1f0935db8afcec2bf345001ebc194422)
 

### Use Github Actions to automate the frontend sync process :

- Create "workspace/Rakefile" & "workspace/Gemfile"
- Create the workflow file "github/workflows/sync.yaml.example"
- Create sync role CFN template and deploy script
- Create "aws/cfn/sync/template.yaml"
- Create "aws/cfn/sync/config.toml"
- Create "bin/cfn/sync"
 - Add policy
```sh
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "s3:PutObject",
                "s3:GetObject",
                "s3:ListBucket",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::shopili.net/*",
                "arn:aws:s3:::shopili.net"
            ]
        }
    ]
}
````
 [Commit](https://github.com/Mossaaba/aws-bootcamp-cruddur-2023/commit/ca607b226260aeee4e5247c01b6e4253947b4a79)

---
## Testing CodePipeline
---
## Refactor the reply popup window  
---
## Setup decorator to handle JWT verification
---
## Authentication in ***NotificationsFeed.js***  
---
## Refactor Backend ***app.py***
---
## Refactor Reply Form
---
## Home activities page updates
---
## Implement Error Handling
---
## Refining Activities and Replies
---
## Updating with production databases
---
### Now let's test the messaging system between our two users!
### Let's re-deploy the service stack and check the new DDB variable in task definition  
### Create machine-user with appropriate permissions on DynamoDB
### Messages are now rendered successfully!
