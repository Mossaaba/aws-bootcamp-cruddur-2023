# Week 5 — DynamoDB and Serverless Caching


>> Why we use ``DynamoDB`` : because we are using direct messaging 

- The hardest thing is the data modelling (How we structure data , which is the primary key … )
- Local Secondary Index LSI
- Global secondary Index GSI
- Use patterns to get informations 
- How we use Partition key , sort key 
- Write all the patterns that I could use , there look at the best commun pattern
- Use limite to get part of data for table scan 
- Prefix PK : MSG#{message_user_uuid} , GRP#{user_uuid}
- Prefix SK MSG#{create_at} , GRP#{last_reply_at}

>> Flow of data with schema is so important
- Single Table design ou separate table 
- I model the data to how application will interact with in it


# Sécurity best practices for DynamoDB

- Type of access to DynamoDb : 
* Internet getaway
* VPC/Gateway
* Accelerator (DAX) with cash
* Cross Account

> ``Server Side``

- Use VPC
- Use DaynamoDB in the same region
- Use CloudTrail

> ``Client side ``

- Use Ham roles/ congnito
- DynamoDB users lifeCycle management
- DAX service role to have only access to DynamoDBB
- Not have acesss to intenet
- Client sie encyptions

# Utility Scrips

```sh 
#Add boto3 
pip install -r requirements.txt
```
 [boto3 documentation ](https://boto3.amazonaws.com/v1/documentation/api/latest/guide/dynamodb.html)
 
 
 
 1. Create a table : 

<img width="1680" alt="Capture d’écran 2023-03-26 à 16 49 31" src="https://user-images.githubusercontent.com/11331502/227783867-cfac6257-7f6c-412f-b206-35189491224c.png">


2. List tales : 

<img width="1680" alt="Capture d’écran 2023-03-26 à 16 51 53" src="https://user-images.githubusercontent.com/11331502/227784223-e60ada6d-90d9-4d35-8741-31d90e0810e8.png">

3. Drop table : 

<img width="1680" alt="Capture d’écran 2023-03-26 à 17 06 56" src="https://user-images.githubusercontent.com/11331502/227789989-9c07e516-0bfd-431a-9135-342dd51d0257.png">


4. Seed Data : 

<img width="1680" alt="Capture d’écran 2023-03-26 à 18 27 53" src="https://user-images.githubusercontent.com/11331502/227789961-d93e305c-70b8-4fc4-b01e-37c242cfa4c5.png">

5. Scan 

<img width="1680" alt="Capture d’écran 2023-03-26 à 18 31 58" src="https://user-images.githubusercontent.com/11331502/227790236-634ffe85-d7b5-4769-980f-26db93d62828.png">

<img width="1680" alt="Capture d’écran 2023-03-26 à 18 34 44" src="https://user-images.githubusercontent.com/11331502/227790315-75cdeb9b-5844-4f97-a3bd-799e81993b06.png">

6. Get conversation 

<img width="1680" alt="Capture d’écran 2023-03-26 à 18 40 54" src="https://user-images.githubusercontent.com/11331502/227790702-8c721ad7-233e-4881-bf59-c8a3e296b9c2.png">


7. List conversations : 

<img width="1680" alt="Capture d’écran 2023-03-26 à 19 00 38" src="https://user-images.githubusercontent.com/11331502/227791863-7eadf455-adb2-43a7-b921-d69c94791da0.png">


8. Cognito list users 
![Capture d’écran 2023-03-29 à 00 20 36](https://user-images.githubusercontent.com/11331502/228379856-f70952b0-3745-4cf7-8ac1-586a5c9e4879.png)


9. Update users Ids 
![Capture d’écran 2023-03-29 à 00 29 30](https://user-images.githubusercontent.com/11331502/228381152-8248441c-c1fd-493f-85f6-74483dae275e.png)

10. Immplementing list of message 
![Capture d’écran 2023-03-30 à 00 46 11](https://user-images.githubusercontent.com/11331502/228684722-40d988df-c067-40e9-a7d4-c5a9ae4fd4da.png)

11. Test sending message 
![Capture d’écran 2023-03-30 à 00 47 18](https://user-images.githubusercontent.com/11331502/228684817-a76834d1-3eb7-4256-9bfd-b212ed4c6059.png)

<img width="1680" alt="Capture d’écran 2023-03-26 à 18 31 58" src="https://user-images.githubusercontent.com/11331502/227790236-634ffe85-d7b5-4769-980f-26db93d62828.png">

12. Test creating new message group 
![Capture d’écran 2023-03-30 à 00 48 09](https://user-images.githubusercontent.com/11331502/228684966-592c3a53-45c0-481a-b7c3-be4e503fa760.png)



13. DynamoDB Stream :

13.1. Add Lambda function 
![Capture d’écran 2023-03-31 à 18 25 48](https://user-images.githubusercontent.com/11331502/229177389-664e7615-a8d7-4782-ab54-27893efc13c0.png)

13.2. Grant permissions 

![Capture d’écran 2023-03-31 à 18 26 10](https://user-images.githubusercontent.com/11331502/229177505-3ce6d540-fc8f-45f7-a585-572bfbc44513.png)

13.3. Create trigger
![Capture d’écran 2023-03-31 à 18 01 02](https://user-images.githubusercontent.com/11331502/229177650-a798d1ba-b4d0-4352-a013-bd887f1ab22b.png)

![Capture d’écran 2023-03-31 à 18 26 25](https://user-images.githubusercontent.com/11331502/229177451-54952e67-599d-4e5f-b30f-7502a1a7b1f0.png)

13.3 Run the lambda 
![Capture d’écran 2023-03-31 à 18 00 37](https://user-images.githubusercontent.com/11331502/229177775-7d178487-c183-403f-a12f-967129f4dbe5.png)

![Capture d’écran 2023-03-31 à 18 00 47](https://user-images.githubusercontent.com/11331502/229177682-9a6e2df2-db07-4546-b17f-35848978b9c1.png)

13.4 Getting log 

![Capture d’écran 2023-03-31 à 18 26 00](https://user-images.githubusercontent.com/11331502/229177585-33483299-319e-430c-9fa0-760e562d2f4d.png)
