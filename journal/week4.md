# Week 4 — Postgres and RDS

RDS is service to create and manage relational DB on AWS cloud. 

>> Auroa is fully managed. 

RDS propose a lot of instance.

### Provision RDS Instance on AWS 

```sh 
CLI CREATE RDS INSATNCE : 

aws rds create-db-instance \
  --db-instance-identifier cruddur-db-instance \
  --db-instance-class db.t3.micro \
  --engine postgres \
  --engine-version  14.6 \
  --master-username cruddurroot \
  --master-user-password goodDataBasePassword23 \
  --allocated-storage 20 \
  --backup-retention-period 0 \
  --port 5432 \
  --no-multi-az \
  --db-name cruddur \
  --storage-type gp2 \
  --publicly-accessible \
  --storage-encrypted \
  --enable-performance-insights \
  --performance-insights-retention-period 7 \
  --no-deletion-protection



Endpoint : cruddur-db-instance.cfoazdtv0rxt.eu-west-3.rds.amazonaws.com

export CONNECTION_URL="postgresql://postgres:password@127.0.0.1:5432/cruddur"
gp env CONNECTION_URL="postgresql://postgres:password@127.0.0.1:5432/cruddur"

export PROD_CONNECTION_URL="postgres://cruddurroot:goodDataBasePassword23@cruddur-db-instance.cfoazdtv0rxt.eu-west-3.rds.amazonaws.com:5432/cruddur" 
gp env PROD_CONNECTION_URL="postgresql://cruddurroot:goodDataBasePassword23@cruddur-db-instance.cfoazdtv0rxt.eu-west-3.rds.amazonaws.com:5432/cruddur" 

```
![Capture d’écran 2023-03-17 à 12 41 02](https://user-images.githubusercontent.com/11331502/225894914-44788503-9560-4d5a-a719-6dc64b841c77.png)

<img width="1680" alt="Capture d’écran 2023-03-17 à 12 41 50" src="https://user-images.githubusercontent.com/11331502/225895019-8c56b6e8-1944-4cd7-a903-6db60ee3cca3.png">

>> Stop this instance parmanently.


### Have a lecture about data modelling in (3rd Normal Form) 3NF for SQL

### Launch Postgres locally via a container


1. Start the docker-compose locally : 
```sh
psql -Upostgres --host localhost
```

![Capture d’écran 2023-03-11 à 19 01 17](https://user-images.githubusercontent.com/11331502/225898692-ce511116-14dc-4e9f-a5e1-b0c5b9f50e0c.png)



2. Commun SQL commande and queries 


```sql
\x on -- expanded display when looking at data
\q -- Quit PSQL
\l -- List all databases
\c database_name -- Connect to a specific database
\dt -- List all tables in the current database
\d table_name -- Describe a specific table
\du -- List all users and their roles
\dn -- List all schemas in the current database
CREATE DATABASE database_name; -- Create a new database
DROP DATABASE database_name; -- Delete a database
CREATE TABLE table_name (column1 datatype1, column2 datatype2, ...); -- Create a new table
DROP TABLE table_name; -- Delete a table
SELECT column1, column2, ... FROM table_name WHERE condition; -- Select data from a table
INSERT INTO table_name (column1, column2, ...) VALUES (value1, value2, ...); -- Insert data into a table
UPDATE table_name SET column1 = value1, column2 = value2, ... WHERE condition; -- Update data in a table
DELETE FROM table_name WHERE condition; -- Delete data from a table

```


### Seed our Postgres Database table with data


1. Create DB 

```SQL
CREATE database cruddur;
````
![Capture d’écran 2023-03-11 à 19 02 19](https://user-images.githubusercontent.com/11331502/225899889-313028fa-d718-4c6d-804d-c409bfad5c3e.png)


- UUID generating random ID

```SQL
CREATE EXTENSION "uuid-ossp";
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
```

2. Create DB with schema SQL and bash scripting : 


![Capture d’écran 2023-03-17 à 13 37 39](https://user-images.githubusercontent.com/11331502/225906767-483e4e20-4c8b-4978-8036-f00287a818db.png)


- Create folder for scripts DB-connect 

![Capture d’écran 2023-03-11 à 19 17 55](https://user-images.githubusercontent.com/11331502/225900165-06a17268-f903-4c3a-85f1-42de1b5e43db.png)


```sh
psql cruddur < db/schema.sql -h localhost -U postgres
```

- Test DDL scripts 

* Delete : 

![Capture d’écran 2023-03-11 à 19 24 03](https://user-images.githubusercontent.com/11331502/225900850-9c3eb60f-03d5-46d1-b55d-87c567624f25.png)

* Create : 

![Capture d’écran 2023-03-11 à 19 28 15](https://user-images.githubusercontent.com/11331502/225900888-699fee84-bf61-4519-8dff-11a9e5adb970.png)


* Load schema 

![Capture d’écran 2023-03-17 à 13 37 39](https://user-images.githubusercontent.com/11331502/225907849-080ee742-84db-4ed9-af61-6aae269ba911.png)

* Viex DB and tables 

![Capture d’écran 2023-03-17 à 13 47 26](https://user-images.githubusercontent.com/11331502/225908939-e7e9891d-61cc-4ea8-a929-8b3000132f66.png)

* Seed Data 

![Capture d’écran 2023-03-17 à 14 16 26](https://user-images.githubusercontent.com/11331502/225915712-e7816a1d-7f2b-4e19-8f10-f51c67239f75.png)

* Session data 

![Capture d’écran 2023-03-17 à 15 11 32](https://user-images.githubusercontent.com/11331502/225929344-e47a46e9-f0ae-452b-a067-184e70ba25af.png)


* DB setup 

![Capture d’écran 2023-03-17 à 15 17 07](https://user-images.githubusercontent.com/11331502/225930601-3ece1e9b-5dbf-4065-bd6e-ab5f45076074.png)



### Write a Postgres adapter
 Add to requirement : 
 
 ```sh
 psycopg[binary]
psycopg[pool]

 ```


- 'pool to handl connection , reuse connection with pool'.


* Add SQL Query for home_activity 

```sh
 sql = query_wrap_array("""
        SELECT
          activities.uuid,
          users.display_name,
          users.handle,
          activities.message,
          activities.replies_count,
          activities.reposts_count,
          activities.likes_count,
          activities.reply_to_activity_uuid,
          activities.expires_at,
          activities.created_at
        FROM public.activities
        LEFT JOIN public.users ON users.uuid = activities.user_uuid
        ORDER BY activities.created_at DESC
        """)

      print(sql)
      with pool.connection() as conn:
        with conn.cursor() as cur:
          cur.execute(sql)
          # this will return a tuple
          # the first field being the data
          json = cur.fetchone()
      return json[0]

```

![Capture d’écran 2023-03-17 à 15 54 40](https://user-images.githubusercontent.com/11331502/225940607-070571bd-3a66-4293-b1c4-97c1c21340a8.png)


### Write an SQL read query


### Write an SQL write query


### Configure VPC Security Groups


### Configure local backend application to use production connection URL


- Add environnemnts variales : 

```sh
export PROD_CONNECTION_URL="postgres://cruddurroot:goodDataBasePassword23@cruddur-db-instance.cfoazdtv0rxt.eu-west-3.rds.amazonaws.com:5432/cruddur" 
gp env PROD_CONNECTION_URL="postgresql://cruddurroot:goodDataBasePassword23@cruddur-db-instance.cfoazdtv0rxt.eu-west-3.rds.amazonaws.com:5432/cruddur" 
````


- Get the IP ADRESS for gitPod : 
```sh 

GITPOD_IP=$(curl ifconfig.me)

echo $GITPOD_IP
````
![Capture d’écran 2023-03-17 à 17 07 21](https://user-images.githubusercontent.com/11331502/225958562-65b2d153-f789-44c8-8799-d72a78f11d20.png)


-- Add inbuded rules 

![Capture d’écran 2023-03-17 à 17 09 32](https://user-images.githubusercontent.com/11331502/225958915-3d5f312f-b09b-485a-a00c-a3b8ee509228.png)

-- Sucess conection 

![Capture d’écran 2023-03-17 à 17 10 31](https://user-images.githubusercontent.com/11331502/225959142-a1525388-6f8f-4c7c-a738-62e28fd05c38.png)

- Update rule 

``sh 
aws ec2 modify-security-group-rules \
    --group-id $DB_SG_ID \
    --security-group-rules "SecurityGroupRuleId=$DB_SG_RULE_ID,SecurityGroupRule={Description=GITPOD,IpProtocol=tcp,FromPort=5432,ToPort=5432,CidrIpv4=$GITPOD_IP/32}"
``

### Implement create activity 

![Capture d’écran 2023-03-17 à 21 07 25](https://user-images.githubusercontent.com/11331502/226022537-667be14a-2cfa-435d-a334-2f5cd2c78367.png)



### Add a caching layer using Momento Serverless Cache
### Propagate metrics from DDB to an RDS metrics table 
